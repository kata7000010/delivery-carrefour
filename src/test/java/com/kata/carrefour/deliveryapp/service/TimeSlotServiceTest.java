package com.kata.carrefour.deliveryapp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;
import com.kata.carrefour.deliveryapp.model.TimeSlotDate;
import com.kata.carrefour.deliveryapp.model.User;
import com.kata.carrefour.deliveryapp.repository.DeliveryRepository;
import com.kata.carrefour.deliveryapp.repository.TimeSlotRepository;
import com.kata.carrefour.deliveryapp.repository.UserRepository;
import com.kata.carrefour.deliveryapp.service.impl.TimeSlotServiceImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TimeSlotServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private DeliveryRepository deliveryRepository;

    @Mock
    private TimeSlotRepository timeSlotRepository;

    @InjectMocks
    private TimeSlotServiceImpl timeSlotService;


    @Test
    public void find_all_should_return_list_time_slot() {
        // Mocking the behavior of userRepository.findUserByUserName()
        User user = new User();
        user.setUserName("snait");
        userRepository.save(user);

        // Mocking the behavior of deliveryRepository.findDeliveryByDeliveryMode()
        Delivery delivery = new Delivery();
        delivery.setDeliveryMode(DeliveryModeEnum.DELIVERY);
        deliveryRepository.save(delivery);

        // Mocking the behavior of timeSlotRepository.findAll()
        List<TimeSlotDate> timeSlots = new ArrayList<>();
        TimeSlotDate timeSlotDate_1 = new TimeSlotDate();
        timeSlotDate_1.setDay("Monday");
        timeSlotDate_1.setTime("10:00");
        timeSlotDate_1.setDeliveryMode(delivery);
        timeSlotDate_1.setUser(user);
        given(timeSlotRepository.findAll()).willReturn(Lists.list(timeSlotDate_1));
        List<TimeSlotDate> timeSlotDates = timeSlotService.getAllTimeSlots();

        // then
        then(timeSlotRepository).should().findAll();
        assertEquals(1, timeSlotDates.size());
    }


    @Test
    public void save_should_call_save__repo_when_timeSlot_not_null_and_return_time_slot() throws Exception {
        // Given
        User user = new User();
        user.setUserName("snaitali");

        Delivery delivery = new Delivery();
        delivery.setDeliveryMode(DeliveryModeEnum.DELIVERY_ASAP);


        TimeSlotDate timeSlotDate = new TimeSlotDate();
        timeSlotDate.setDay("Friday");
        timeSlotDate.setTime("21:00");
        timeSlotDate.setUser(user);
        timeSlotDate.setDeliveryMode(delivery);


        given(userRepository.findUserByUserName(any()))
                .willReturn(Optional.of(user));
        given(deliveryRepository.findDeliveryByDeliveryMode(any())).willReturn(delivery);
        given(timeSlotRepository.save(any(TimeSlotDate.class))).willReturn(timeSlotDate);


        // when
        TimeSlotDate timeSlotDateSaved = timeSlotService.reserveDeliveryTimeSlot(timeSlotDate);

        // Then
        assertThat(timeSlotDateSaved).isNotNull();
        then(timeSlotRepository).should().save(timeSlotDate);

    }

}
