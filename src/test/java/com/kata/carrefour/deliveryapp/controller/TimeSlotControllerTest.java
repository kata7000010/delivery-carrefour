package com.kata.carrefour.deliveryapp.controller;

import com.kata.carrefour.deliveryapp.dto.TimeSlotDto;
import com.kata.carrefour.deliveryapp.service.TimeSlotService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.then;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@AutoConfigureJsonTesters
public class TimeSlotControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TimeSlotService timeSlotService;

    @Autowired
    private JacksonTester<List<TimeSlotDto>> jsonTimeSlotDto;

    @Test
    public void when_Apply_find_All_should_return_list_time_slot() throws Exception {

        TimeSlotDto timeSlotDto_1 = new TimeSlotDto();
        timeSlotDto_1.setTime("09:00");
        timeSlotDto_1.setDay("Lundi");
        timeSlotDto_1.setUsername("snait");
        timeSlotDto_1.setDeliveryMode("DELIVERY_ASAP");

        TimeSlotDto timeSlotDto_2 = new TimeSlotDto();
        timeSlotDto_2.setTime("07:00");
        timeSlotDto_2.setDay("Mardi");
        timeSlotDto_2.setUsername("snaitali");
        timeSlotDto_2.setDeliveryMode("DELIVERY");

        List<TimeSlotDto> timeSlotDtos = Lists.list(timeSlotDto_1, timeSlotDto_2);

        // when
        MockHttpServletResponse response = mvc.perform(get("/api/time-slot").accept(MediaType.APPLICATION_JSON)).andReturn()
                .getResponse();

        // then
        then(timeSlotService).should().getAllTimeSlots();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

    }
}
