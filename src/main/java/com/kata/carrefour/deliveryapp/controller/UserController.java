package com.kata.carrefour.deliveryapp.controller;

import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.mapper.UserMapper;
import com.kata.carrefour.deliveryapp.model.User;
import com.kata.carrefour.deliveryapp.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getUsers() {

        return ResponseEntity.ok(UserMapper.mapEntitiesToDtos(userService.getAllUsers()));
    }

    @PostMapping
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto userDto) {
        return ResponseEntity.ok(UserMapper.mapEntityToDto(userService.addUser(UserMapper.mapDtoToEntity(userDto))));
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserDto> getUserByUsername(@PathVariable String username) throws Exception {
        return ResponseEntity.ok(UserMapper.mapEntityToDto(userService.findUserByUsername(username)));
    }
}
