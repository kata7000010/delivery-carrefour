package com.kata.carrefour.deliveryapp.controller;

import com.kata.carrefour.deliveryapp.dto.DeliveryDto;
import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.mapper.DeliveryMapper;
import com.kata.carrefour.deliveryapp.mapper.UserMapper;
import com.kata.carrefour.deliveryapp.service.DeliveryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/delivries")
public class DeliveryController {
    private final DeliveryService deliveryService;

    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @GetMapping
    public ResponseEntity<List<DeliveryDto>> getDeliveryModes() {

        return ResponseEntity.ok(DeliveryMapper.mapEntitiesToDtos(deliveryService.getAllDeliveryModes()));
    }
}
