package com.kata.carrefour.deliveryapp.controller;

import com.kata.carrefour.deliveryapp.dto.TimeSlotDto;
import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.mapper.TimeSlotMapper;
import com.kata.carrefour.deliveryapp.mapper.UserMapper;
import com.kata.carrefour.deliveryapp.model.TimeSlotDate;
import com.kata.carrefour.deliveryapp.service.TimeSlotService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/time-slot")
public class TimeSlotController {
    private final TimeSlotService timeSlotService;

    public TimeSlotController(TimeSlotService timeSlotService) {
        this.timeSlotService = timeSlotService;
    }

    @GetMapping
    public ResponseEntity<List<TimeSlotDto>> getTimeSlots() {

        return ResponseEntity.ok(TimeSlotMapper.mapEntitiesToDtos(timeSlotService.getAllTimeSlots()));
    }

    @PostMapping
    public ResponseEntity<TimeSlotDto> reserve(@RequestBody TimeSlotDto timeSlotDto) throws Exception {
        TimeSlotDate timeSlotDate = timeSlotService.reserveDeliveryTimeSlot(TimeSlotMapper.mapDtoToEntity(timeSlotDto));
        return ResponseEntity.ok(TimeSlotMapper.mapEntityToDto(timeSlotDate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTimeSlot(@PathVariable Long id) {
        timeSlotService.deleteTimeSlot(id);
        return ResponseEntity.ok().build();
    }
}
