package com.kata.carrefour.deliveryapp.mapper;

import com.kata.carrefour.deliveryapp.dto.TimeSlotDto;
import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;
import com.kata.carrefour.deliveryapp.model.TimeSlotDate;
import com.kata.carrefour.deliveryapp.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class TimeSlotMapper {
    public static TimeSlotDate mapDtoToEntity(TimeSlotDto timeSlotDto) {
        TimeSlotDate timeSlotDate = new TimeSlotDate();
        timeSlotDate.setDay(timeSlotDto.getDay());
        timeSlotDate.setTime(timeSlotDto.getTime());
        timeSlotDate.setUser(mapUser(timeSlotDto.getUsername()));
        timeSlotDate.setDeliveryMode(mapDeliveryMode(timeSlotDto.getDeliveryMode()));
        return timeSlotDate;
    }

    public static TimeSlotDto mapEntityToDto(TimeSlotDate timeSlotDate) {
        if (timeSlotDate == null) {
            return null;
        }

        TimeSlotDto timeSlotDto = new TimeSlotDto();
        timeSlotDto.setDay(timeSlotDate.getDay());
        timeSlotDto.setTime(timeSlotDate.getTime());

        timeSlotDto.setDeliveryMode(mapDeliveryToEnum(timeSlotDate.getDeliveryMode()));
        timeSlotDto.setUsername(mapUserToUsername(timeSlotDate.getUser()));

        return timeSlotDto;
    }

    public static List<TimeSlotDto> mapEntitiesToDtos(List<TimeSlotDate> timeSlotDates) {
        List<TimeSlotDto> timeSlotDtos = timeSlotDates.stream().map(timeSlotDate -> {
            return mapEntityToDto(timeSlotDate);
        }).toList();
        return timeSlotDtos;
    }

    private static String mapUserToUsername(User user) {
        return user.getUserName();
    }

    private static String mapDeliveryToEnum(Delivery deliveryMode) {
        return deliveryMode.getDeliveryMode().getValue();
    }

    private static Delivery mapDeliveryMode(String deliveryMode) {
        Delivery delivery = new Delivery();
        delivery.setDeliveryMode(DeliveryModeEnum.valueOf(deliveryMode));
        return delivery;
    }

    private static User mapUser(String username) {
        User user = new User();
        user.setUserName(username);
        return user;
    }
}
