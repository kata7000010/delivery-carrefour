package com.kata.carrefour.deliveryapp.mapper;

import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {
    public static User mapDtoToEntity(UserDto dto) {
        User user = new User();
        user.setUserName(dto.getUsername());
        return user;
    }

    public static UserDto mapEntityToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUserName());
        return userDto;
    }

    public static List<UserDto> mapEntitiesToDtos(List<User> users) {
        List<UserDto> userDtos = users.stream().map(user -> {
            UserDto userDto = new UserDto();
            userDto.setUsername(user.getUserName());
            return userDto;
        }).collect(Collectors.toList());
        return userDtos;
    }
}
