package com.kata.carrefour.deliveryapp.mapper;

import com.kata.carrefour.deliveryapp.dto.DeliveryDto;
import com.kata.carrefour.deliveryapp.dto.UserDto;
import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;
import com.kata.carrefour.deliveryapp.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class DeliveryMapper {

    public static Delivery mapDtoToEntity(DeliveryDto dto) {
        Delivery delivery = new Delivery();
        delivery.setDeliveryMode(DeliveryModeEnum.valueOf(dto.getDeliveryMode()));
        return delivery;
    }

    public static DeliveryDto mapEntityToDto(Delivery delivery) {
        DeliveryDto deliveryDto = new DeliveryDto();
        deliveryDto.setDeliveryMode(delivery.getDeliveryMode().getValue());
        return deliveryDto;
    }

    public static List<DeliveryDto> mapEntitiesToDtos(List<Delivery> deliveries) {
        List<DeliveryDto> deliveryDtos = deliveries.stream().map(delivery -> {
            DeliveryDto deliveryDto = new DeliveryDto();
            deliveryDto.setDeliveryMode(delivery.getDeliveryMode().getValue());
            return deliveryDto;
        }).collect(Collectors.toList());
        return deliveryDtos;
    }
}
