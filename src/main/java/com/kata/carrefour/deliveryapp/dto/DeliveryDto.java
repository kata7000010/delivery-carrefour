package com.kata.carrefour.deliveryapp.dto;

import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;

public class DeliveryDto {
    private String deliveryMode;

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }
}
