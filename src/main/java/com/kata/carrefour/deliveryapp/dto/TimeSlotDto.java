package com.kata.carrefour.deliveryapp.dto;

import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;

public class TimeSlotDto {
    private String day;
    private String time;
    private String deliveryMode;
    private String username;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
