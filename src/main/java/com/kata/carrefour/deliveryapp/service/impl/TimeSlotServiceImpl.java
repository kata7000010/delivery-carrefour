package com.kata.carrefour.deliveryapp.service.impl;

import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.TimeSlotDate;
import com.kata.carrefour.deliveryapp.model.User;
import com.kata.carrefour.deliveryapp.repository.DeliveryRepository;
import com.kata.carrefour.deliveryapp.repository.TimeSlotRepository;
import com.kata.carrefour.deliveryapp.repository.UserRepository;
import com.kata.carrefour.deliveryapp.service.TimeSlotService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TimeSlotServiceImpl implements TimeSlotService {

    private final TimeSlotRepository timeSlotRepository;
    private final UserRepository userRepository;
    private final DeliveryRepository deliveryRepository;

    public TimeSlotServiceImpl(TimeSlotRepository timeSlotRepository, UserRepository userRepository, DeliveryRepository deliveryRepository) {
        this.timeSlotRepository = timeSlotRepository;
        this.userRepository = userRepository;
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public List<TimeSlotDate> getAllTimeSlots() {
        return timeSlotRepository.findAll();
    }

    @Override
    @Transactional
    public TimeSlotDate reserveDeliveryTimeSlot(TimeSlotDate timeSlotDate) throws Exception {
        if (timeSlotDate == null) {
            throw new Exception("compte rendu cannot be null");
        }
        timeSlotDate.setUser(findUserByUsername(timeSlotDate.getUser()));
        Delivery delivery = deliveryRepository.findDeliveryByDeliveryMode(timeSlotDate.getDeliveryMode().getDeliveryMode());
        timeSlotDate.setDeliveryMode(delivery);

        return timeSlotRepository.save(timeSlotDate);
    }

    @Transactional
    public User findUserByUsername(User user) throws Exception {
        if (user == null) {
            return null;
        }
        if (user.getUserName() == null) {
            return null;
        }
        return userRepository.findUserByUserName(user.getUserName()).orElseThrow(() -> new Exception("User not found"));

    }

    @Override
    @Transactional
    public void deleteTimeSlot(Long timeSlotId) {
        timeSlotRepository.deleteById(timeSlotId);
    }
}
