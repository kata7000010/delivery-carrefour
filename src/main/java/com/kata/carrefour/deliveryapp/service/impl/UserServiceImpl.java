package com.kata.carrefour.deliveryapp.service.impl;

import com.kata.carrefour.deliveryapp.model.User;
import com.kata.carrefour.deliveryapp.repository.UserRepository;
import com.kata.carrefour.deliveryapp.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findUserByUsername(String username) throws Exception {
        return userRepository.findUserByUserName(username).orElseThrow(() -> new Exception("User not found"));
    }
}
