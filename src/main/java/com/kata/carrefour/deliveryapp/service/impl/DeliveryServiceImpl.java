package com.kata.carrefour.deliveryapp.service.impl;

import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.repository.DeliveryRepository;
import com.kata.carrefour.deliveryapp.service.DeliveryService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DeliveryServiceImpl implements DeliveryService {
    private final DeliveryRepository deliveryRepository;

    public DeliveryServiceImpl(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public List<Delivery> getAllDeliveryModes() {
        return deliveryRepository.findAll();
    }

    @Override
    public Delivery addDeliveryMode(Delivery delivery) {
        return deliveryRepository.save(delivery);
    }
}
