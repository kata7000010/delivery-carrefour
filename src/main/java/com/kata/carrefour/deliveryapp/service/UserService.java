package com.kata.carrefour.deliveryapp.service;

import com.kata.carrefour.deliveryapp.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User addUser(User user);
    User findUserByUsername(String username) throws Exception;
}
