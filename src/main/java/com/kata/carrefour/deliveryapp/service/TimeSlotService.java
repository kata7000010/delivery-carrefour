package com.kata.carrefour.deliveryapp.service;

import com.kata.carrefour.deliveryapp.model.TimeSlotDate;

import java.util.List;

public interface TimeSlotService {
    List<TimeSlotDate> getAllTimeSlots();
    TimeSlotDate reserveDeliveryTimeSlot(TimeSlotDate timeSlotDate) throws Exception;


    void deleteTimeSlot(Long timeSlotId);
}
