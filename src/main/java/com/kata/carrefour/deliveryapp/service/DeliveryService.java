package com.kata.carrefour.deliveryapp.service;

import com.kata.carrefour.deliveryapp.model.Delivery;

import java.util.List;

public interface DeliveryService {
    List<Delivery> getAllDeliveryModes();
    Delivery addDeliveryMode(Delivery delivery);
}
