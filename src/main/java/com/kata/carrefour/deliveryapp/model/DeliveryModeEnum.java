package com.kata.carrefour.deliveryapp.model;

public enum DeliveryModeEnum {
    DRIVE("DRIVE"),
    DELIVERY("DELIVERY"),
    DELIVERY_TODAY("DELIVERY_TODAY"),
    DELIVERY_ASAP("DELIVERY_ASAP");

    private final String value;

    DeliveryModeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
