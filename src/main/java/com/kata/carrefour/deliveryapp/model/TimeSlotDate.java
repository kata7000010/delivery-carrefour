package com.kata.carrefour.deliveryapp.model;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "_TIME_SLOT")
public class TimeSlotDate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`DAY`")
    private String day;

    @Column(name = "TIME")
    private String time;

    @ManyToOne
    private Delivery deliveryMode;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Delivery getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(Delivery deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
