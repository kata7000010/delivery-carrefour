package com.kata.carrefour.deliveryapp.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "_USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String userName;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<TimeSlotDate> timeSlotDates;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<TimeSlotDate> getTimeSlotDates() {
        return timeSlotDates;
    }

    public void setTimeSlotDates(Set<TimeSlotDate> timeSlotDates) {
        this.timeSlotDates = timeSlotDates;
    }
}
