package com.kata.carrefour.deliveryapp;

import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;
import com.kata.carrefour.deliveryapp.model.User;
import com.kata.carrefour.deliveryapp.service.DeliveryService;
import com.kata.carrefour.deliveryapp.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class DeliveryAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeliveryAppApplication.class, args);
    }

    @Bean
    CommandLineRunner run(UserService userService, DeliveryService deliveryService) {
        return args -> {
            Stream.of("snait", "testUsername1", "testUsername2").forEach(username-> {
                User user1 = new User();
                user1.setUserName(username);
                userService.addUser(user1);
            });
            DeliveryModeEnum[] deliveryModes = DeliveryModeEnum.values();
            Stream.of(deliveryModes).forEach(deliveryMode -> {
                Delivery delivery = new Delivery();
                delivery.setDeliveryMode(deliveryMode);
                deliveryService.addDeliveryMode(delivery);
            });
        };
    }

}
