package com.kata.carrefour.deliveryapp.repository;

import com.kata.carrefour.deliveryapp.model.TimeSlotDate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeSlotRepository extends JpaRepository<TimeSlotDate, Long> {
}
