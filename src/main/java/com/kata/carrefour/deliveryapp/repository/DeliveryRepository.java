package com.kata.carrefour.deliveryapp.repository;

import com.kata.carrefour.deliveryapp.model.Delivery;
import com.kata.carrefour.deliveryapp.model.DeliveryModeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {
    Delivery findDeliveryByDeliveryMode(DeliveryModeEnum deliveryModeEnum);
}
