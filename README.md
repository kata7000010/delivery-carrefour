# Mon Projet

Ce projet est une application qui consiste à gérer les "Delivery carrefour".
-----------------
pour lancer l'application:  attention à la version de java!  java 21
-------------------

### Documentation de l'API REST:
pour obtenir le swagger:
http://localhost:8080/swagger-ui/index.html

### Gestion des Dépendances

Avant de lancer le projet, assurez-vous d'avoir installé les dépendances nécessaires. Utilisez Maven avec la commande
suivante :
 - mvn install

### Configuration de la Base de Données
L'application utilise une base de données H2 pour les tests. Ajustez la configuration de la base
de données dans le fichier src/main/resources/application.properties en conséquence.

### Exécution des Tests
Exécutez les tests unitaires et d'intégration avant de déployer ou de contribuer au projet. Utilisez la commande
suivante pour exécuter les tests :
 - mvn test